/******************************************************************************
 * ROFI Color theme
 * User: DeanPdx
 * Copyright: Dean Davidson
 ******************************************************************************/

* {
    /**** Solarized colors ****
    * NOTE: I am including all of them even though I am using a sub-set. I keep
    * tweaking my theme and having access to all of the colors here is handy. */

    base03: #002b36;
    base02: #073642;
    base01: #586e75;
    base00: #657b83;
    base0: #839496;
    base1: #93a1a1;
    base2: #eee8d5;
    base3: #fdf6e3;
    yellow: #b58900;
    orange: #cb4b16;
    red: #dc322f;
    violet: #6c71c4;
    blue: #268bd2;
    cyan: #2aa198;
    green: #859900;
    bg: #1F2333;
    fg: #f7eeed;
    primary: #C17392;
    primary-under: #CD8EA7;
    secondary: #939EB4;
    secondary-under: #ABB4C4;
    tertiary: #C7D8D3;
    tertiary-under: #DBE6E3;
    alert: #EF476F;
    alert-under: #F26989;

    /* Solarized Darker Theme */
    transparent-background: rgba ( 0, 0, 0, 0 % );
    foreground:                  @fg;
    background:                  @bg;

    normal-foreground:           @fg;
    normal-background:           @transparent-background;
    active-foreground:           @fg;
    active-background:           @transparent-background;
    urgent-foreground:           @fg;
    urgent-background:           @transparent-background;

    selected-normal-foreground:  @bg;
    selected-normal-background:  @primary;
    selected-active-foreground:  @green;
    selected-active-background:  @primary;
    selected-urgent-foreground:  @red;
    selected-urgent-background:  @primary;

    alternate-normal-foreground: @foreground;
    alternate-normal-background: @transparent-background;
    alternate-active-foreground: @active-foreground;
    alternate-active-background: @transparent-background;
    alternate-urgent-foreground: @urgent-foreground;
    alternate-urgent-background: @transparent-background;

    separatorcolor:              @primary;

    /* Main element styles */
    border-color:                @primary-under;
    spacing:                     2;
    background-color:            @transparent-background;
}
window {
    background-color: @background;
    border:           2;
    padding:          10;
}
mainbox {
    border:  0;
    padding: 0;
}
message {
    border:       1px dash 0px 0px ;
    border-color: @separatorcolor;
    padding:      1px ;
}
textbox {
  padding: 1px;
    text-color: @tertiary;
}
listview {
    fixed-height: 0;
    border:       1px dash 0px 0px ;
    border-color: @separatorcolor;
    spacing:      2px ;
    scrollbar:    false;
    padding:      8px 0px 0px 0px;
}
element {
    border:  0;
    padding: 1px 0px 1px 1px;
}
element normal.normal {
    background-color: @normal-background;
    text-color:       @normal-foreground;
}
element normal.urgent {
    background-color: @urgent-background;
    text-color:       @urgent-foreground;
}
element normal.active {
    background-color: @active-background;
    text-color:       @active-foreground;
}
element selected.normal {
    background-color: @selected-normal-background;
    text-color:       @selected-normal-foreground;
}
element selected.urgent {
    background-color: @selected-urgent-background;
    text-color:       @selected-urgent-foreground;
}
element selected.active {
    background-color: @selected-active-background;
    text-color:       @selected-active-foreground;
}
element alternate.normal {
    background-color: @alternate-normal-background;
    text-color:       @alternate-normal-foreground;
}
element alternate.urgent {
    background-color: @alternate-urgent-background;
    text-color:       @alternate-urgent-foreground;
}
element alternate.active {
    background-color: @alternate-active-background;
    text-color:       @alternate-active-foreground;
}
scrollbar {
    width:        4px ;
    border:       0;
    handle-width: 8px ;
    padding:      0;
}
mode-switcher {
    border:       2px dash 0px 0px ;
    border-color: @separatorcolor;
}
button selected {
    background-color: @selected-normal-background;
    text-color:       @selected-normal-foreground;
}
inputbar {
    spacing:    1;
    text-color: @tertiary;
    padding:    1px ;
}
case-indicator {
    spacing:    1;
    text-color: @normal-foreground;
}
entry {
    spacing:    1;
    text-color: @normal-foreground;
}
prompt {
    enabled:false;
}
